"""
This module implements the various file system operations that are
used in the file management system software.

The following operations should be implemented:-
1. Move a folder and it's contents from one folder to another
2. Move a specific file from one folder to another
3. Move all files with a specific file extension from one folder
to another
4. Move all files that match a given file pattern to one folder
5.
"""
import shutil
import os
import os.path
from os import walk


def move(src, dest, cpy_function=None):
    """
    Wrapper function for shutil's move function, with the exception
    that it does not have a return value. This wrapping avoids the
    need to import shutil for move operations beyond this module
    """
    if cpy_function is None:
        cpy_function = shutil.copy2
    shutil.move(src, dest, cpy_function)


def move_ext(ext, src, dest):
    """
    Moves all files in src that have an ext extension to the dest folder
    """
    files = []
    # Get all the non-directory files in the src folder
    for (dirpath, dirnames, filenames) in walk(src):
        files.extend(filenames)
        break
    # Successively move each file from the src folder to the dest folder
    src = os.path.normpath(src)
    dest = os.path.normpath(dest)

    for file in files:
        if file.endswith(ext) or file.endswith(ext.upper()):
            src_path = os.path.join(src, file)
            dest_path = os.path.join(dest, file)
            shutil.move(src_path, dest_path)


def transfer_progress(src, dest):
    """
    Determines the percentage of the src that has been copied to dest
    """



"""
Need a function that will:
1. Report on the progress of a file move/copy operation given the src directory
and the destination directory. 
2. The function should be called everytime after certain amount of data has been
moved/copied
3. The return value of this function can be used in a progress bar
4. Cannot use the default copyfunction used by shutil.move()
"""


class FileSystem:
    def __init__(self):
        pass
