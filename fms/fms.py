import tkinter as tk
from fms.ui.actionbar import ActionBar
from fms.ui.browseframe import BrowseFrame
from fms.ui.checkframe import CheckFrame
from fms.fms_config import f_extensions
from fms.fms_config import f_groups
import fms.fs as fs


class Fms(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("File Management System")
        self.resizable(False, False)
        self.browse_frame = BrowseFrame(self, relief="groove")
        self.check_frame = CheckFrame(self, relief="groove", options=f_extensions, option_group=f_groups, cols=2)
        self.action_bar = ActionBar(self)
        self.init_layout()

    def init_layout(self):
        self.browse_frame.grid(row=0, column=0)
        self.check_frame.grid(row=1, column=0, sticky="W")
        self.action_bar.grid(row=2, column=0, sticky=tk.E)

    def run(self):
        self.configure_event_handlers()
        self.mainloop()

    def configure_event_handlers(self):
        self.action_bar.cancel_btn.config(command=self.destroy)
        self.action_bar.clean_btn.config(command=self.move_action)

    def move_action(self):
        on_states = [state for state in self.check_frame.get_states() if state == 1]
        if len(on_states) == 0:
            fs.move(self.browse_frame.src_path.get(), self.browse_frame.dest_path.get())
        else:
            states = self.check_frame.get_states()
            extensions = [f_extensions[i][1] for i in range(len(f_extensions)) if states[i] != 0]
            for ext in extensions:
                fs.move_ext(ext, self.browse_frame.src_path.get(), self.browse_frame.dest_path.get())


if __name__ == "__main__":
    app = Fms()
    app.run()
