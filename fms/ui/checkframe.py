import tkinter as tk
from tkinter import ttk
from math import ceil


class CheckFrame(ttk.Frame):
    def __init__(self, parent=None, relief=None, options=None, option_group=None, cols=None):
        super().__init__(parent)
        self.option_groups = []
        self.states = []
        self.frames = []
        if cols is None:
            cols = 1
        self.cols = cols
        self.notebook = ttk.Notebook(self)
        self.init_layout()
        self.group_options(options, option_group)
        self.layout_option_groups(relief)
        self.layout_options()

    def init_layout(self):
        self.notebook.pack(padx=(10, 10), pady=(10, 10))

    def group_options(self, options, option_groups):
        """
        Groups different options into categories based on the categries
        in option_group parameter of the constructor
        """
        for i in range(0, len(option_groups)):
            self.option_groups.append([])
            for option in options:
                if option[0] == option_groups[i]:
                    self.option_groups[i].append(option)
        return self.option_groups

    def layout_option_groups(self, relief=None):
        """
        Creates a frame for each group of files and adds it to a note book
        """
        for i in range(len(self.option_groups)):
            frame = ttk.Frame(self.notebook, relief=relief)
            self.frames.append(frame)
            self.notebook.add(self.frames[i], text=self.option_groups[i][0][0])

    def layout_options(self):
        """
        Creates a check button for every option in the options parameter passed
        in the constructor and uses the grid layout manager to arrange these
        options within their respective options groups
        """
        for i in range(len(self.option_groups)):
            # Number of rendered checkbox rows is dependent on the size of the
            # options available to this widget, as well as the columns that
            # these should occupy
            row_count = ceil(len(self.option_groups[i]) / float(self.cols))
            for row in range(row_count):
                for col in range(self.cols):
                    # Create a state variable for checkbox
                    state = tk.IntVar()
                    # Mapping between list index and grid index for check options
                    index = row * self.cols + col
                    if index < len(self.option_groups[i]):
                        option = ttk.Checkbutton(self.frames[i], text="*." + self.option_groups[i][index][1],
                                                 variable=state)
                        option.grid(row=row, column=col, sticky=tk.W)
                        self.states.append(state)
                    else:
                        break

    def get_states(self):
        """
        :return: A list of current states for each rendered in this frame
        """
        return list(map(lambda state: state.get(), self.states))
