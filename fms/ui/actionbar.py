import tkinter as tk
from tkinter import ttk


class ActionBar(ttk.Frame):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.cancel_btn = ttk.Button(self, text="Cancel")
        self.clean_btn = ttk.Button(self, text="Move")
        self.cancel_btn.grid(row=0, column=0, padx=(5, 5), pady=(5, 10))
        self.clean_btn.grid(row=0, column=1, padx=(5, 10), pady=(5, 10))
