import tkinter as tk
from tkinter import ttk
from tkinter import filedialog


class BrowseFrame(ttk.Frame):
    def __init__(self, parent=None, width=None, height=None, relief=None):
        super().__init__(parent, width=width, height=height, relief=relief)
        self.browse_src_label = ttk.Label(self, text="Source:")
        self.browse_dest_label = ttk.Label(self, text="Destination:")
        self.src_path = tk.StringVar()
        self.dest_path = tk.StringVar()
        self.browse_src = ttk.Entry(self, width=40, textvariable=self.src_path)
        self.browse_dest = ttk.Entry(self, width=40, textvariable=self.dest_path)
        self.browse_src_btn = ttk.Button(self, text="Browse")
        self.browse_dest_btn = ttk.Button(self, text="Browse")
        self.layout_widgets()
        self.configure_event_handlers()

    def layout_widgets(self):
        self.browse_src_label.grid(row=0, column=0, padx=(10, 10), pady=(20, 0), sticky="W")
        self.browse_src.grid(row=0, column=1, padx=(10, 10), pady=(20, 0))
        self.browse_src_btn.grid(row=0, column=2, sticky="W", padx=(0, 10), pady=(20, 10))

        self.browse_dest_label.grid(row=1, column=0, padx=(10, 10), pady=(10, 10))
        self.browse_dest.grid(row=1, column=1, padx=(10, 10), pady=(10, 10))
        self.browse_dest_btn.grid(row=1, column=2, sticky="E", padx=(0, 10), pady=(10, 10))

    def configure_event_handlers(self):
        self.browse_src_btn.config(command=lambda: self.src_path.set(tk.filedialog.askdirectory()))
        self.browse_dest_btn.config(command=lambda: self.dest_path.set(tk.filedialog.askdirectory()))
